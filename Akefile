sub refresh-meta { use JSON::Fast; try from-json slurp 'META6.json' }

my $CONFIG = BEGIN { use TOML::Thumb; try from-toml slurp 'dist.toml' }
my $META   = BEGIN refresh-meta;
my $NOW    = BEGIN DateTime.now: formatter => {
    sprintf '%sT%s+%02d:00',
        .yyyy-mm-dd,
        .hh-mm-ss,
        .offset-in-hours.Int;
}

sub bump ( $next ) {
    for $META<provides>.values».IO {
        .spurt: .slurp.subst: ":ver<$META<version>>", ":ver<$next>";
        run < git add >, $_;
    }

    .spurt: .slurp.subst: $META<version>, $next given 'META6.json'.IO;
    run < git add META6.json >;
}

sub do-download {
    use HTTP::Tiny;

    %*ENV<HTTP_TINY_DEBUG> = 1;

    given HTTP::Tiny.new {
        my $res = .mirror:
            'https://publicsuffix.org/list/public_suffix_list.dat',
            'resources'.IO.child('public_suffix_list.dat');

        exit 0 unless $res<status> eq 200; # No changes

        my $mirror = .mirror:
            'https://raw.githubusercontent.com/publicsuffix/list/master/tests/tests.txt',
            'resources'.IO.child('tests.txt');

        given run < git status --porcelain >, :out {
            unless .out.lines.grep: * eq ' M resources/public_suffix_list.dat' {
                # We caught a false bump
                dd $res;
                dd $mirror;
                exit 0;
            }
        }
    }
}

sub do-update {
    my $date = 'resources/public_suffix_list.dat'.IO.modified.DateTime;

    bump do {
        my @parts = $META<version>.split: '.';
        @parts.pop;
        @parts.push: $date.yyyy-mm-dd.subst( '-', :g );
        @parts.join: '.';
    }

    .spurt:
        .slurp.subst(
            '{{$NEXT}}' ~ "\n",
            '{{$NEXT}}' ~ "\n    - Update public suffix list\n"
        ) given 'Changes'.IO;

    run < git add resources Changes >;
    run « git commit -m "Update public suffix list" »;

    $META = refresh-meta;
    execute 'release';
    execute 'upload';
}

sub do-clean {
    my $name = S:g/'::'/-/ given $CONFIG<name> // $META<name>;

    use File::Find;
    use File::Directory::Tree;

    for sort find
        dir     => '.',
        name    => *.starts-with("$name-") {

        next unless .e;
        say "Removing $_";
        .d ?? .&rmtree !! .unlink;
    }
}

sub do-test {
    with Proc::Async.new: < prove6 -l > {
        react {
            whenever .Supply { say '  > ' ~ $_ for .split: "\n" }
            whenever .start { }
        }
    }
}

sub do-build {
    my $name   = S:g/'::'/-/ given $CONFIG<name> // $META<name>;
    my $target = "$name-{ $META<version> }".IO;

    use File::Ignore;
    use File::Find;
    $target.mkdir;

    my $git    = File::Ignore.new: rules => '.gitignore'.IO.slurp.lines;
    my $ignore = File::Ignore.new: rules => $CONFIG<build><exclude> // Empty;

    for sort { .f, .chars },
        find dir => '.', exclude => *.basename.starts-with: '.' {

        when .d {
            # Ignore empty directories
            next unless .dir: test => { not .starts-with('.') }

            # Ignore if on exclude list
            next if $git.ignore-directory: $_;
            next if $ignore.ignore-directory: $_;

            $target.child($_).mkdir;
        }

        when .f {
            # Ignore if on exclude list
            next if $git.ignore-file: $_;
            next if $ignore.ignore-file: $_;

            try .copy: $target.child($_);
        }
    }

    my $label = join ' - ', $META<version>, $NOW;

    .spurt: .slurp.subst( '{{$NEXT}}', "$label\n" )
        given $target.child: 'Changes';
}

sub do-archive {
    my $name   = S:g/'::'/-/ given $CONFIG<name> // $META<name>;
    my $target = "$name-{ $META<version> }".IO;

    say "Archiving as $target.tar.gz";
    run « tar czf "$target.tar.gz" $target »;
}

sub do-release {
    my $label = join ' - ', $META<version>, $NOW;
    my $name  = S:g/'::'/-/ given $CONFIG<name> // $META<name>;
    my $build = "$name-{ $META<version> }".IO;

    # Make release commit

    $build.child('Changes').copy: '.'.IO.child('Changes');

    run < git add Changes >;
    run < git commit -m >, $CONFIG<bump><commit_message> // "Release v$META<version>";
    run « git tag "v$META<version>" »;

    # Make bump commit

    .spurt: .slurp.subst( $label, '{{$NEXT}}' ~ "\n\n$label" ) given 'Changes'.IO;

    bump %*ENV<NEXT_VERSION>
        // ( Version.new( $META<version> ).parts Z+ ( 0, 0, 1 ) ).join: '.';

    run < git add Changes >;
    run < git commit -m >, $CONFIG<bump><commit_message> // "Bump version";

    # Ensure nothing broke
    execute 'check';
    execute 'test';
}

sub do-check-workspace {
    CATCH { default { .message.note; exit 1 } }

    given $META {
        my $name = S:g/'::'/-/ given "{ .<name> }-{ .<version> }";

        die "There is already a build directory for { .<name> }:ver<{ .<version> }>"
            if $name.IO.e;

        die "There is already a tarball for { .<name> }:ver<{ .<version> }> ($name.gz)"
            if "$name.gz".IO.e;
    }

    say '  Workspace looks good';
}

sub do-check-changes {
    say '  Checking changes';

    CATCH { default { .message.note; exit 1 } }

    die 'No Changes file' unless 'Changes'.IO.e;

    given 'Changes'.IO.slurp {
        die 'Change log does not have {{$NEXT}} placeholder'
            when !/ ^^ '{{$NEXT}}' $$ /;
    }

    say '  Changes look good';
}

sub do-check-meta {
    CATCH { default { .message.note; exit 1 } }

    die 'Missing META6.json' unless 'META6.json'.IO.e;

    given $META {
        die "Bad version: { .<version> }"
            unless .<version> ~~ / ^ [ \d+ ] ** 3 % '.' $ /;

        die "Bad auth: '{ .<auth> }' should start with 'zef:'"
           unless .<auth>.starts-with: 'zef:';

        for .<provides>.kv -> $key, $value {
            for $value».IO -> $path {
                next unless $path.slurp ~~ / ':ver<' $<version> = ( <-[>]>+ ) '>' /;

                die "Version in $path ({ $<version> }) does not match the one in META ({ .<version> })"
                    unless ~$<version> eq .<version>;
            }

            use lib '.';
            my $meta = $_;
            die "Could not load {$key}:ver<{ .<version> }>:auth<{ .<auth> }>"
                unless $*REPO.repo-chain
                    .map({ .?candidates( $key, ver => $meta<version>, auth => $meta<auth> ) })
                    .flat
                    .first: *.defined;
        }

        die "Missing resource from META: $_" unless 'resources'.IO.child($_).e
            for ( .<resources> // () ).List;

        with .<source-url> {
            die .err.slurp if .exitcode
                given run « git ls-remote $_ », :out, :err;
        }
    }

    say '  META looks good';
}

sub do-upload {
    my $tarball = "{ $META<name> }-{ $META<version> }.tar.gz".IO;
    die "No tarball to upload!" unless $tarball.e;

    if %*ENV<PUBLIC_SUFFIX_AUTORELEASE> {
        run < fez upload --unattended >, "--file=$tarball";
    }
    else {
        say "All set to release { $META<name> } v{ $META<version> } with";
        say "    zef upload --file=$tarball";
        say '';
        say "Not doing it myself because you didn't set PUBLIC_SUFFIX_AUTORELEASE";
    }
}

task 'default' => < release >;

task 'clean', &do-clean;
task 'test',  &do-test;

task 'check' => < check-workspace check-changes check-meta >;

task 'check-workspace', &do-check-workspace;
task 'check-changes',   &do-check-changes;
task 'check-meta',      &do-check-meta;

task 'release' => < clean check test archive >, &do-release;

task 'archive' => < build >, &do-archive;
task 'build'   => < clean >, &do-build;

task 'download', &do-download;
task 'update'  => < clean check test download >, &do-update;
task 'upload' => < archive >, &do-upload;
