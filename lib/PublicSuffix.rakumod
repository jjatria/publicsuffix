class X::PublicSuffix::BadDomain is Exception {
    has $!host    is built;
    has $!message is built;

    method message {
        "Invalid host name '{ $!host.gist }': it $!message";
    }
}

package PublicSuffix:ver<0.1.20250307>:auth<zef:jjatria> {
    # Parse file once and cache on compilation
    my constant %RULES = gather for %?RESOURCES<public_suffix_list.dat>.lines {
        next if .not or .starts-with: '//';
        take ~$<rule> => 'e' when / ^ '!'  $<rule> = \S+ /;
        take ~$<rule> => 'w' when / ^ '*.' $<rule> = \S+ /;
        take ~$<rule> => 'i' when / ^      $<rule> = \S+ /;
    }

    my sub to-unicode ( Str:D $_ --> Str ) is hidden-from-backtrace {
        use IDNA::Punycode;
        lc .contains('xn--') ?? .split('.')».&decode_punycode.join: '.' !! $_
    }

    my sub to-ascii ( Str:D $_ --> Str ) is hidden-from-backtrace {
        use IDNA::Punycode;
        .contains('xn--') ?? $_ !! .split('.')».&encode_punycode.join: '.'
    }

    my sub validate ( Str $_ --> True ) is hidden-from-backtrace {
        # As per https://datatracker.ietf.org/doc/html/rfc1035#section-2.3.4
        # See also https://devblogs.microsoft.com/oldnewthing/?p=7873

        my $message;
        $message ||= do 'must be a non-empty string'         if .not;
        $message ||= do 'must not start with period (.)'     if .starts-with: '.';
        $message ||= do 'must not have more than 253 octets' if .encode.elems > 253;
        $message ||= do 'must have labels with between 1 and 63 octets'
            unless .split('.')».encode».elems.all ~~ 1 .. 63;

        X::PublicSuffix::BadDomain.new( host => $_, :$message).throw if $message;
    }

=begin pod

=head2 NAME

PublicSuffix - Query Mozilla's Public Suffix List

=head2 SYNOPSIS

=begin code :lang<raku>
use PublicSuffix;

# The effective TLD of a host name
say public-suffix 'www.example.com';
# OUTPUT: com

# New TLDs are valid public suffix by default
say public-suffix 'www.example.unknownnewtld';
# OUTPUT: unknownnewtld

# Accept host names in Unicode
say public-suffix 'www.example.香港';
# OUTPUT: 香港

# Accept host names in punycode
say public-suffix 'www.example.xn--j6w193g';
# OUTPUT: xn--j6w193g

# Shortest domain that can be registered
say registrable-domain 'www.example.com';
# OUTPUT: example.com

# Returns a type object if registrable domain is not found
say registrable-domain 'com';
# OUTPUT: (Str)
=end code

=head2 DESCRIPTION

This module provides functions to query Mozilla's
L<Public Suffix List|http://publicsuffix.org>: a community-maintained
list of domain name suffixes. The data in this list can be used to determine
the effective top-level domain of a host name, or to test whether two hosts
share an origin, as well as other similar validation functions. This most
commonly used when validating the scope of HTTP cookies to prevent
L<supercookies|https://en.wikipedia.org/wiki/Supercookie>, but it has
L<a variety of other uses|https://publicsuffix.org/learn>.

=head2 FUNCTIONS

=head3 Host name validation

The functions described below take host names as their parameter, and run
some validation on their input before processing. When given a malformed or
otherwise invalid host name, the functions below will throw a
X::PublicSuffix::BadDomain exception with the reason for the failure as its
C<message>.

For a domain to be valid, it must be a non-empty string, with a maximum
length of 253 octets, and no more than 63 octets per label.

Domains can be provided as either UTF-8 strings or their ASCII punycoded
variants. The returned strings will use the format of the strings provided.
In other words, if you provide a UTF-8 string, you will receive a UTF-8
string back, while giving an ASCII string will generate an ASCII string in
return.

Providing partially punycoded strings is not supported, and the behaviour
of these functions with that input is undefined.

=head3 public-suffix

=begin code :lang<raku>
sub public-suffix ( Str $host ) returns Str
=end code

Takes a host name as a Str and returns the
L<public suffix|https://url.spec.whatwg.org/#host-public-suffix> for that
host, or the type object if no public-suffix is found or if the host is the
string representation of a IPv4 or IPv6 address. This function will throw a
X::PublicSuffix::BadDomain exception if the host name is not valid.

According to
L<§ 3.2 of the URL living standard|https://url.spec.whatwg.org/#host-miscellaneous>,
the public suffix is "the portion of a host which is included on the Public
Suffix List".

=end pod

    our sub public-suffix ( Str $host is copy --> Str ) is export {
        validate $host;

        return Str
            if $host.contains(':')                               # IPv6: contains a ':'
            || $host ~~ /^ [ <[ 0..9 ]> ** 1..3 ] ** 4 % '.' $/; # IPv4

        my $punycode = ( $host // '' ).contains: 'xn--';
        $host .= &to-unicode;

        my $suffix;
        while $host {
            my $rhs = $host.split( '.', 2 ).tail;
            my $top = $host eq $rhs;

            $suffix = do with %RULES{$host} {
                when 'i' { $host                }
                when 'w' { $top ?? $host !! Str }
                default  { $rhs                 }
            }
            elsif $host eq $rhs {
                # We are down to a single part that matches no rules
                # The prevailing rule is the default '*' wildcard
                $rhs.split('.').tail;
            }
            elsif %RULES{$rhs} -> $_ {
                when 'w' { $host                     }
                when 'i' { $rhs                      }
                default  { $rhs.split( '.', 2 ).tail }
            }

            last if $suffix || $top;

            $host = $rhs;
        }

        $punycode ?? $suffix.&to-ascii !! $suffix;
    }

=begin pod

=head3 registrable-domain

=begin code :lang<raku>
sub registrable-domain ( Str $host ) returns Str
=end code

Takes a host name as a Str and returns the
L<registrable domain|https://url.spec.whatwg.org/#host-registrable-domain>
for that host, or the type object if no registrable domain is found or if the
host is the string representation of a IPv4 or IPv6 address. This function
will throw a X::PublicSuffix::BadDomain exception if the host name is not
valid.

According to
L<§ 3.2 of the URL living standard|https://url.spec.whatwg.org/#host-miscellaneous>,
the registrable domain of a host is "the most specific public suffix, along
with the domain label immediately preceding it, if any".

=end pod

    our sub registrable-domain ( Str $host is copy --> Str ) is export {
        validate $host;

        my $punycode = ( $host // '' ).contains: 'xn--';
        $host .= &to-unicode;

        my $tld = public-suffix $host;
        return Str unless $tld && $host ne $tld;

        my $rest = $host.substr( 0, * - $tld.chars - 1 ).split('.').tail;

        $punycode ?? .&to-ascii !! $_ given "$rest.$tld"
    }
}

=begin pod

=head2 AUTHOR

José Joaquín Atria <jjatria@cpan.org>

=head2 ACKNOWLEDGEMENTS

The code in this distribution takes inspiration from a number of similar
Perl libraries. In particular:

=item L<Mozilla::PublicSuffix|https://metacpan.org/pod/Mozilla::PublicSuffix>

=item L<IO::Socket::SSL::PublicSuffix|https://metacpan.org/pod/IO::Socket::SSL::PublicSuffix>

In addition to the distributions mentioned above, the API was mostly
inspired by the L<publicsuffixlist|https://pypi.org/project/publicsuffixlist>
Python module by ko-zu.

This module owes a debt of gratitude to their authors and those who have
contributed to them, and to their choice to make their code and work
publicly available.

=head2 COPYRIGHT AND LICENSE

Copyright 2022 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it
under the Artistic License 2.0.

=end pod
