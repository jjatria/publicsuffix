#!/usr/bin/env raku

# Tests in this file ure the ones written for this distribution.
# For upstream tests see `t/external.t`.

use Test;
use PublicSuffix;

is public-suffix('ck'), 'ck', 'literal match on wildcard rule';

subtest 'Synopsis' => {
    is public-suffix('www.example.com'),
        'com',
        'The effective TLD of a host name';

    is public-suffix('www.example.unknownnewtld'),
        'unknownnewtld',
        'New TLDs are valid public suffix by default';

    is public-suffix('www.example.香港'),
        '香港',
        'Accept host names in Unicode';

    is public-suffix('www.example.xn--j6w193g'),
        'xn--j6w193g',
        'Accept host names in punycode';

    is registrable-domain('www.example.com'),
        'example.com',
        'Shortest domain that can be registered';

    is registrable-domain('com'),
        Str,
        'Returns a type object if registrable domain is not found';
}

done-testing;
